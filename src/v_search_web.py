import html
from time import sleep
from threading import Thread
from flask import Flask, render_template, request, session, copy_current_request_context
from v_search import search_for_letters
from config import config
from DB_cm import DatabaseContextManager, DatabaseConnectionError, DatabaseCredentialsError, SQLError
from login_checker import check_logged_in

app = Flask(__name__)
app.config['database'] = config['database']
app.secret_key = config['secret_key']


@app.route('/login')
def login_page() -> str:
    session[config['login_key']] = True
    return 'You logged in'


@app.route('/logout')
def logout_page() -> str:
    del session[config['login_key']]
    return 'You logged out'


@app.route('/')
@app.route('/entry')
def open_entry() -> html:
    return render_template('entry.html', the_title='Search for letters')


@app.route('/search', methods=['POST'])
def do_search() -> html:
    @copy_current_request_context
    def log_request(req: request, res: str) -> None:
        # Mimic SQL transaction delay
        sleep(15)
        # Context Manager returns cursor
        with DatabaseContextManager(app.config['database']) as cursor:
            # CreateSQL request
            _SQL = """INSERT INTO logs
                          (phrase, letters, ip, browser_string, results)
                          VALUES
                          (%s, %s, %s, %s, %s)"""
            # Send SQL request
            cursor.execute(_SQL, (req.form['phrase'],
                                  req.form['letters'],
                                  req.remote_addr,
                                  req.user_agent.browser,
                                  res))

    phrase = request.form['phrase']
    letters = request.form['letters']
    title = 'Results'
    result = search_for_letters(phrase, letters)

    try:
        t = Thread(target=log_request, args=(request, str(result)))
        t.start()
    except DatabaseConnectionError as err:
        print('log_request failed with DatabaseConnectionError:', err)
    except DatabaseCredentialsError as err:
        print('log_request failed with DatabaseCredentialsError:', err)
    except SQLError as err:
        print('open_logs failed with SQLError:', err)

    return render_template(
        'results.html',
        the_title=title,
        the_phrase=phrase,
        the_letters=letters,
        the_results=result
    )


@app.route('/logs')
@check_logged_in
def open_logs() -> str:
    try:
        with DatabaseContextManager(app.config['database']) as cursor:
            _SQL = """ SELECT phrase, letters, ip, browser_string, results FROM logs"""
            cursor.execute(_SQL)
            data = cursor.fetchall()

        titles = ('Phrase', 'Letters', 'Remote addr', 'User agent', 'Results')

        return render_template(
            'logs.html',
            the_title='Logs',
            titles=titles,
            data=data
        )
    except DatabaseConnectionError as err:
        print('open_logs failed with DatabaseConnectionError:', err)
    except DatabaseCredentialsError as err:
        print('open_logs failed with DatabaseCredentialsError:', err)
    except SQLError as err:
        print('open_logs failed with SQLError:', err)
    except Exception as err:
        print(f'Something went wrong: {err}')

    return render_template(
        'logs.html',
        the_title='Logs',
        titles=('System message',),
        data=[['ERROR: NO DATA']]
    )


if __name__ == '__main__':
    app.run(debug=True)
