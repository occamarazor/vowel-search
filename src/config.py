config = {
    'secret_key': 'VerySecretKey',
    'login_key': 'logged_in',
    'logged_out_message': 'You are not logged in!',
    'database': {
        'host': '1.1.1.1',
        'user': 'myuser',
        'password': 'mypassword',
        'database': 'mydatabase'
    }
}
