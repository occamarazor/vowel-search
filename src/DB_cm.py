import mysql.connector


class DatabaseConnectionError(Exception):
    pass


class DatabaseCredentialsError(Exception):
    pass


class SQLError(Exception):
    pass


# Context manager (implementing context management protocol)
class DatabaseContextManager:
    # 1. Called before [__enter__] to create attrs
    def __init__(self, config: dict) -> None:
        self.configuration = config
        self.connection = None
        self.cursor = None

    # 2. Called before executing [WITH body] to return a value to be assigned by [WITH]
    def __enter__(self) -> 'cursor':
        try:
            # Establish DB connection
            self.connection = mysql.connector.connect(**self.configuration)
            # Create a cursor (DB descriptor to send SQL commands and receive results)
            self.cursor = self.connection.cursor()
            return self.cursor
        except mysql.connector.errors.ProgrammingError as err:
            raise DatabaseCredentialsError(err)
        except mysql.connector.errors.DatabaseError as err:
            raise DatabaseConnectionError(err)

    # Called after executing [WITH body] to finish up
    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        if exc_type is mysql.connector.errors.ProgrammingError:
            raise SQLError(exc_val)
        # Force write cached data into a table
        self.connection.commit()
        # Close cursor
        self.cursor.close()
        # Close connection
        self.connection.close()
        if exc_type:
            raise exc_type(exc_val)
