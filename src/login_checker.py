from flask import session
from functools import wraps
from typing import Callable
from config import config


def check_logged_in(func: Callable) -> Callable:
    @wraps(func)
    def wrapper(*args, **kwargs) -> str:
        if config['login_key'] in session:
            return func(*args, **kwargs)
        return config['logged_out_message']

    return wrapper
