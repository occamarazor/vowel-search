# Simple Flask App
###### Overview: Search for letters in a phrase, write and read request logs  
- Log in through **/login** page  
- Log out through **/logout** page  

## Runbook
#### Set Environment
- Install Homebrew package manager  
    `ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`  
- Update and upgrade Homebrew **(if needed)**  
    `brew update && brew upgrade && brew cleanup`
- Install python 3.x, pipenv and MariaDB  
    `brew install python pipenv mariadb`  
- Uninstall pip global packages **(if needed)**  
    `pip3 freeze > to_delete.txt`  
    `pip3 uninstall -y -r to_delete.txt`  
    `rm to_delete.txt`  
- Create each custom module distribution package  
    `cd modules/[MODULE_NAME]`  
    `python setup.py sdist`  
- Create new venv with all project dependencies  
    `pipenv install`  
- Add Pipenv interpreter for current project  

#### Set DB
- Start MariaDB server  
    `mysql.server start`  
- Enter root user shell  
    `sudo su`  
- Set mysql root user password  
    `mysqladmin -u root password`  
- Enter as mysql root user  
    `mysql -u root`  
- Create DB  
    `CREATE DATABASE mydatabase;`  
- Create a %USER_NAME% and grant privileges on %DB_NAME%  
    `GRANT ALL ON mydatabase.* TO myuser@localhost IDENTIFIED BY 'mypassword';`  
- Leave mysql and root user shell  
    `exit`  
    `exit`  
- Access mysql as %USER_NAME%  
    `mysql -u myuser -p`  
- Use %DB_NAME%  
    `USE mydatabase;`  
- Create logs table  
    `CREATE TABLE logs`  
    `(`  
    `  id             INT AUTO_INCREMENT PRIMARY KEY,`  
    `  ts             TIMESTAMP DEFAULT current_timestamp,`  
    `  phrase         VARCHAR(128) NOT NULL,`  
    `  letters        VARCHAR(32)  NOT NULL,`  
    `  ip             VARCHAR(16)  NOT NULL,`  
    `  browser_string VARCHAR(256) NOT NULL,`  
    `  results        VARCHAR(64)  NOT NULL`  
    `);`  
- Add data source to project
- Edit app db credentials in [app config](./src/config.py)

#### Dev mode
- Run v_search_web.py  

#### Prod mode
- _Not available yet_


## Troubleshooting
###### If mysql server won't stop (ERROR: A mysqld process already exists)  
1. Kill all mysqld process:  
    `killall mysqld mysqld_safe`  
2. Wait at least 10 seconds for clean shut down  
3. Check whether any mysqld process still running  
    `ps ax | grep mysql`  
4. If processes > 1:  
    `killall -9 mysqld mysqld_safe`  
5. Start mysql server again  
    `mysql.server start`  