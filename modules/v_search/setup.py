from setuptools import setup

setup(
    name='v_search',
    version='1.0',
    description='Custom Search Tool',
    author='Max',
    author_email='abc@mail.ru',
    url='bitbucket.org',
    py_modules=['v_search']
)
