def search_for_vowels(word):
    """
    Searches for vowels in a word
    """
    vowels = set('aeiou')
    return vowels.intersection(word)


def search_for_letters(phrase: str, letters: str = 'aeiou') -> set:
    """
    Searches for a set of letters in a phrase
    """
    return set(letters).intersection(phrase)


# print(search_for_vowels('Sky'))
# print(search_for_letters(letters='yk', phrase='Sky'))
